lxml
requests!=2.32.0,!=2.32.1,!=2.32.2,!=2.32.3,>=2.0.0
python-dateutil
PyYAML
html2text>=3.200
unidecode
Pillow
Babel
packaging~=23.0
pycountry
rich~=13.0
termcolor

[:python_version < "3.8"]
importlib-metadata~=6.7
