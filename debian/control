Source: woob
Section: python
Priority: optional
Maintainer: Romain Bignon <romain@symlink.me>
Build-Depends: debhelper (>= 11~), python3-all (>=3.7), python3-setuptools, dh-python, python3-docutils, python3-sphinx, pybuild-plugin-pyproject
Standards-Version: 4.6.1
X-Python3-Version: >= 3.7
Homepage: https://woob.dev
Vcs-Git: https://gitlab.com/woob/debian.git
Vcs-Browser: https://gitlab.com/woob/debian.git

Package: python3-woob
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, python3-dateutil, python3-lxml,
 python3-yaml, python3-requests (>= 2.0), python3-pil, python3-gnupg,
 python3-unidecode, python3-babel, python3-packaging, python3-pycountry,
 python3-rich
Suggests: woob
Description: Woob, Web Out Of Browsers - Python3 library
 Woob is a project helping interaction between applications and websites.
 .
 This package contains the woob libray.

Package: woob
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, python3-woob (>= ${source:Version}), python3-html2text, python3-prettytable
Recommends: python3-termcolor
Description: Woob, Web Out Of Browsers - Applications
 Woob is a project helping interaction between applications and websites.
 .
 This package contains command-line applications including:
 .
 bands           display bands and suggestions
 bank            manage bank accounts
 bill            get/download documents and bills
 books           manage rented books
 bugtracker      manage bug tracking issues
 calendar        see upcoming events
 cinema          search movies and persons around cinema
 cli             call a method on backends
 config          manage backends or register new accounts
 contentedit     manage websites content
 dating          interact with dating websites
 debug           debug backends
 gallery         browse and download web image galleries
 gauge           display sensors and gauges values
 geolocip        geolocalize IP addresses
 housing         search for housing
 job             search for a job
 lyrics          search and display song lyrics
 money           import bank accounts into Microsoft Money
 msg             send and receive message threads
 parcel          manage your parcels
 paste           post and get pastes from pastebins
 pricecompare    compare products
 radio           search, show or listen to radio stations
 recipes         search and consult recipes
 repos           manage a woob repository
 rpg             manage RPG data
 shop            obtain details and status of e-commerce orders
 smtp            daemon to send and check messages
 subtitles       search and download subtitles
 torrent         search and download torrents
 translate       translate text from one language to another
 travel          search for train stations and departures
 video           search and play videos
 weather         display weather and forecasts
