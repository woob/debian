Metadata-Version: 2.1
Name: woob
Version: 3.7
Summary: Woob, Web Outside Of Browsers
Author-email: Romain Bignon <romain@woob.dev>
License: LGPL-3.0-or-later
Project-URL: Homepage, https://woob.tech
Project-URL: Source, https://gitlab.com/woob/woob
Project-URL: Release notes, https://gitlab.com/woob/woob/-/releases
Project-URL: Documentation, https://woob.dev
Project-URL: Bug Tracker, https://gitlab.com/woob/woob/-/issues
Keywords: scraping,web,banking
Classifier: Environment :: Console
Classifier: License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)
Classifier: Programming Language :: Python
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.7
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Programming Language :: Python :: 3.11
Classifier: Programming Language :: Python
Classifier: Topic :: Internet :: WWW/HTTP
Classifier: Topic :: Internet :: WWW/HTTP :: Browsers
Classifier: Topic :: Software Development :: Libraries :: Application Frameworks
Classifier: Topic :: Software Development :: Libraries :: Python Modules
Classifier: Topic :: Text Processing :: Markup :: HTML
Requires-Python: >=3.7
Description-Content-Type: text/x-rst
License-File: COPYING
License-File: COPYING.LESSER
License-File: AUTHORS
Requires-Dist: lxml
Requires-Dist: requests!=2.32.0,!=2.32.1,!=2.32.2,!=2.32.3,>=2.0.0
Requires-Dist: python-dateutil
Requires-Dist: PyYAML
Requires-Dist: html2text>=3.200
Requires-Dist: unidecode
Requires-Dist: Pillow
Requires-Dist: Babel
Requires-Dist: packaging~=23.0
Requires-Dist: pycountry
Requires-Dist: rich~=13.0
Requires-Dist: termcolor
Requires-Dist: importlib-metadata~=6.7; python_version < "3.8"

====
Woob
====

|version| |last-commit| |python| |license|

.. |version| image:: https://img.shields.io/pypi/v/woob
    :target: https://pypi.org/project/woob/
    :alt: Package Version
.. |last-commit| image:: https://img.shields.io/gitlab/last-commit/woob/woob
    :target: https://gitlab.com/woob/woob/
    :alt: Last commit
.. |python| image:: https://img.shields.io/pypi/pyversions/woob
    :target: https://pypi.org/project/woob/
    :alt: Python Version
.. |license| image:: https://img.shields.io/pypi/l/woob
    :target: https://gitlab.com/woob/woob/-/blob/master/COPYING.LESSER
    :alt: License

Woob (`Web Outside of Browsers`) is a library which provides a Python standardized API and data models to
access websites.

Overview
========

.. image:: https://woob.dev/_images/arch.png

There are three main concepts:

* `Capabilities <https://woob.dev/guides/capabilities>`_: This is a standardized interface
  to access a specific kind of website. It provides an unified API and standard
  datamodels;
* `Modules <https://woob.dev/guides/modules>`_: A module is dedicated to a specific
  website. It can implements several capabilities (for example `paypal <https://paypal.com>`_ module may
  implement ``CapBank`` to get bank
  informations, ``CapTransfer`` to
  initiate a transfer, ``CapProfile`` to get
  information about the customer, and ``CapDocument`` to get documents);
* `Backends <https://woob.dev/guides/user/quickstart>`_: You can load a module several times,
  with different configurations. For example, if you have two PayPal accounts,
  you can create two backends of the same module with different credentials.

The main ``Woob`` class let configure new backends and do aggregated calls to
every backends loaded with a specific capability.

For example, once backends are loaded, you can call ``iter_accounts()`` and
you'll get accounts in the same ``Account`` data model for all backends
implementing ``CapBank``:

.. code-block:: python

   >>> from woob.core import Woob
   >>> from woob.capabilities.bank import CapBank
   >>> w = Woob()
   >>> w.load_backends(CapBank)
   {'societegenerale': <Backend 'societegenerale'>,
    'creditmutuel': <Backend 'creditmutuel'>}
   >>> accounts = list(w.iter_accounts())
   >>> print(accounts)
   [<Account id='7418529638527412' label=u'Compte de ch\xe8ques'>,
    <Account id='9876543216549871' label=u'Livret A'>,
    <Account id='123456789123456789123EUR' label=u'C/C Eurocompte Confort M Roger Philibert'>]
   >>> accounts[0].balance
   Decimal('87.32')


Applications
============

If you are looking for applications using the woob library, visit `woob.tech <https://woob.tech>`_.


Installation
============

Read this `documentation <https://woob.dev/guides/install/>`_.

Documentation
=============

More information about how to use woob at `woob.dev <https://woob.dev>`_.

Contributing
============

If you want to contribute to woob (patch of the core, creating new modules,
etc.), `read this <https://woob.dev/guides/contribute/>`_.

Chat with us
============

* `#woob @ liberachat <ircs://irc.libera.chat/woob>`_
* `#woob @ matrix.org <https://matrix.to/#/#woob:matrix.org>`_
